from utils.functions import (
    ask_question,
    check_player,
    selectionner_mot,
    lancementJeu,
    dessin_Grille,
    position_lettre_dans_mot,
    compte_nb_lettres,
    combien_fois_lettre_dans_mot,
    remplaceDansGrille,
    score,
    add_to_stats_sheet,
    NouvellePartie,
)

def test_compte_nb_lettres():
    assert compte_nb_lettres("ABCD") == 4

    # assert duplicates not counted twice
    assert compte_nb_lettres("ABCDDE") == 5


def test_combien_fois_lettre_dans_mot():
    assert combien_fois_lettre_dans_mot("yo","y" ) == 1