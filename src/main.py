import sys

from utils.functions import (
    ask_question,
    check_player,
    selectionner_mot,
    lancementJeu,
    dessin_Grille,
    position_lettre_dans_mot,
    compte_nb_lettres,
    combien_fois_lettre_dans_mot,
    remplaceDansGrille,
    score,
    add_to_stats_sheet,
    NouvellePartie,
)


def main():

    player_name = ask_question()
    check_player(player_name)
    on_continue = True
    while on_continue is True:
        motATrouver = selectionner_mot()
        tentatives = lancementJeu(motATrouver)
        grille = dessin_Grille(motATrouver)
        nbEchecs = 0
        nbReussites = 0
        missedLetters = []
        while tentatives != 0 and "_" in grille and on_continue is True:
            lettre = input(
                "Quelle lettre voulez vous-jouer\nSi vous voulez quitter merci d\"entrer '\q'"
            ).lower()
            while not ((lettre.isalpha() and len(lettre) == 1) or lettre == "\q"):
                lettre = input(
                    'Ce n"est pas une lettre\nVeuillez-saisir une lettre'
                ).lower()
            if lettre == "\q":
                sys.exit("Vous avez quitté le jeu")
            # lettre = input('Quelle lettre voulez vous-jouer\nSi vous voulez quitter merci d"entrer "\q"').lower()
            positionLettre = position_lettre_dans_mot(motATrouver, lettre)
            nbLettres = compte_nb_lettres(motATrouver)
            compte_nb_lettres(motATrouver)
            lettreApparaitMot = combien_fois_lettre_dans_mot(motATrouver, lettre)
            if lettreApparaitMot > 0:
                grille = remplaceDansGrille(motATrouver, lettre, grille)
                nbLettres -= 1
                nbReussites += 1
                # lettreChoisie = "b"
                if lettreApparaitMot > 1:
                    print(
                        "Bravo, la lettre est située aux positions ",
                        str(positionLettre),
                        " .",
                    )
                    print(" ".join(grille))
                elif lettreApparaitMot == 1:
                    print(
                        "Bravo, la lettre est située à la position ",
                        str(positionLettre),
                        " .",
                    )
                    print(" ".join(grille))
            else:
                print(" ".join(grille))
                nbEchecs += 1
                tentatives -= 1
                missedLetters.append(lettre)
                print("Raté, la lettre " + str(lettre) + " n'est pas dans le mot.")
                print(
                    "Il vous reste "
                    + str(nbLettres - lettreApparaitMot)
                    + " lettre(s) différente(s) à deviner."
                )
                print(
                    "Il vous reste "
                    + str(tentatives)
                    + " tentatives pour deviner le mot."
                )
                print(
                    "Pour rappel vous avez déja joué ces lettres n'appartenant pas au mot\n",
                    missedLetters,
                )
        if motATrouver == "".join(grille):
            # print('Bravo vous avez trouvez le mot "{}"'.format(motATrouver))
            resultat = score(nbReussites, nbEchecs, tentatives, motATrouver)
            motTrouve = "Oui"
            add_to_stats_sheet(player_name, motATrouver, motTrouve, resultat)
            on_continue = NouvellePartie()
        elif motATrouver != "".join(grille):
            # print('Raté vous n\'avez pas trouvé le mot "{}"'.format(motATrouver))
            resultat = score(nbReussites, nbEchecs, tentatives, motATrouver)
            motTrouve = "Non"
            add_to_stats_sheet(player_name, motATrouver, motTrouve, resultat)
            on_continue = NouvellePartie()


if __name__ == "__main__":
    main()
