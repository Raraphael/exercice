import pandas as pd
import random
import sys


def ask_question():
    player_name = input("Quelle est votre pseudo ? ")
    return player_name


def check_player(player_name):
    df = pd.read_csv("liste_resultats_pendu.txt")
    listeDeJoueur = df["Nom_du_joueur"].unique()
    if player_name in listeDeJoueur:
        motsDejaTrouve = list(
            df.loc[(df["Nom_du_joueur"] == player_name) & (df["motTrouve"] == "Oui")][
                "Mot_a_chercher"
            ]
        )
        if len(motsDejaTrouve) == 0:
            print(
                "Salut",
                player_name,
                "tu as déjà joué à ce jeu mais tu n'as trouvé aucun mot",
            )
        else:
            print(
                "Salut",
                player_name,
                "tu as déjà joué à ce jeu voici les mots que tu as déja trouvé\n",
                motsDejaTrouve,
            )
        statistiques = input(
            "Veux-tu des statistiques sur tes scores? Oui-non\n"
        ).lower()
        if statistiques == "oui":
            get_player_stat(player_name)
    else:
        print(
            "Salut {} tu n'as encore jamais joué, commençons. Tes scores seront enregistrés après ta partie".format(
                player_name
            )
        )


def get_player_stat(playerName):
    """
    Nom de la fonction : get_player_stat
    Objet : Récupérer les informations relatives au joueur afin d'en sortir des statistiques
    :Params : le nom du joueur (string)
    :Return : Message statistiques : nb de parties, V-D, Ratio, Score moyen,
    """
    df = pd.read_csv("liste_resultats_pendu.txt")
    listeDeJoueur = df["Nom_du_joueur"].unique()
    df["len_word"] = df["Mot_a_chercher"].apply(lambda x: len(x))
    df_player = df[df["Nom_du_joueur"] == playerName]
    nbParties = len(df[df["Nom_du_joueur"] == playerName])
    victoires = len(
        df.loc[(df["Nom_du_joueur"] == playerName) & (df["motTrouve"] == "Oui")]
    )
    defaites = len(
        df.loc[(df["Nom_du_joueur"] == playerName) & (df["motTrouve"] == "Non")]
    )
    ratio = round(victoires / nbParties, 2)
    df_mots_trouves = df.loc[
        (df["Nom_du_joueur"] == playererName) & (df["motTrouve"] == "Oui")
    ]
    if victoires == 0:
        print(
            "Tu as joué {} parties, tu en a gagnés {} et perdu {} ce qui te fait un ratio de {}%".format(
                nbParties, victoires, defaites, ratio
            )
        )
    else:
        motLePlusLong = list(
            df_mots_trouves.sort_values(by="len_word", ascending=False)[
                "Mot_a_chercher"
            ]
        )[0]
        longueurMot = len(motLePlusLong)
        scoreMoyen = round(df_player["Resultat"].mean(), 2)
        meilleurScore = df_player["Resultat"].max()
        moinsBonScore = df_player["Resultat"].min()
        print(
            "Tu as joué {} parties, tu en a gagnés {} et perdu {} ce qui te fait un ratio de {}%".format(
                nbParties, victoires, defaites, ratio
            )
        )
        print(
            "Le mot le plus long que tu as trouvé était {}, il comportait {} lettres".format(
                motLePlusLong, longueurMot
            )
        )
        print(
            "Tes scores :\nMin:{}\nMax:{}\nMoyen:{}".format(
                moinsBonScore, meilleurScore, scoreMoyen
            )
        )


def selectionner_mot():
    """
    Nom de la fonction : find_word
    Objet : stocker les mots à deviner à partir d'un fichier texte dans une liste et selectionner un mot au hasard
    :Params : Aucun
    :Return : Un mot :string
    """
    with open("list_of_words.txt", encoding="utf-8") as f:
        listeDeMot = [line.rstrip("\n") for line in f]
        word = random.choice(listeDeMot)

        # print("Le mot contient ", len(word), "lettres.")
    # print(("_"+" ") *len(motATrouver))
    # for tiret in word:
    # print("- ", end='')
    return word


# print(motATrouver)


# ajouter les test de lecture et d'existence du fichier
def lancementJeu(mot):
    """
    Nom de la fonction : lancementJeu
    Objet : Affiche la grille de jeu, indiue au joueur le nombre de tentative dont il dispose en fonction de la longueur du mot, lui demande de saisir une lettre et complète le mot le cas échéant avec la lettre trouvée
    :Param : mot choisit au hasard à l'étape précédente
    :Return : la grille remplie si le joueur trouve le mot
    """
    if len(mot) % 2 == 0:
        nbTentative = int(len(mot) / 2)
    else:
        nbTentative = int((len(mot) // 2) + 1)
    print("========Début de la partie========")
    print("Le mot contient ", len(mot), "lettres.")
    grille = []
    grille = ["-" if l == "-" else "_" for l in mot]
    print("Vous avez " + str(nbTentative) + " tentatives, bonne chance !")
    # print(" ".join(grille))
    # print("Vous avez " + str(nbTentative) + " tentatives, bonne chance !")
    return nbTentative


def dessin_Grille(mot):
    grille = []
    grille = ["-" if l == "-" else "_" for l in mot]
    print(" ".join(grille))
    return grille


def combien_fois_lettre_dans_mot(mot, lettre):
    """
    nom : combien_fois_lettre_dans_mot
    objet : calcule combien de fois la lettre choisie est présente dans le mot
    param :  2 str
    return : int
    """
    combienFois = 0

    for l in mot:
        if lettre == l:
            combienFois += 1

    return combienFois


def compte_nb_lettres(mot):
    """
    nom : compte_nb_lettres
    objet : calcule le nombre de lettres contenues dans le mot ne compte pas les doublons
    param : str
    return : int
    """

    mot = set(mot)
    mot = "".join(mot)

    return len(mot)


def position_lettre_dans_mot(mot, lettre):
    """
    nom : combien_fois_lettre_dans_mot
    objet : calcule combien de fois la lettre choisie est présente dans le mot
    param : 2 str
    return : list
    """
    position = []

    for i, l in enumerate(mot):
        if lettre == l:
            position.append(i)
    return position


def remplaceDansGrille(mot, lettre, grille):
    grille = [ltr if ltr == lettre else grille[i] for i, ltr in enumerate(mot)]
    return grille


def score(reussites, echecs, nbTentatives, motATrouver):
    """
    Nom de la fonction : score
    Objet : Calculer le score du joueur selon son nombre de tentatives
    :Params =3 : nbReussites, nbEchecs, nbTentatives (integer)
    :Return : le score du joueur
    """
    if nbTentatives != 0:
        if echecs == 0:
            resultat = 10
            print(
                "Bravo vous avez gagné sans vous trompez, vous avez", resultat, "points"
            )
        else:
            resultat = (-2) * echecs
            print(
                "Vous avez réussi mais vous vous êtes trompéz {} fois, vous avez {} points".format(
                    echecs, resultat
                )
            )
    else:
        resultat = int(echecs * (-2) - 5)
        print(
            'Raté vous n"avez pas trouvé le mot : "{}", vous avez {} points'.format(
                motATrouver, resultat
            )
        )
    return str(resultat)


def add_to_stats_sheet(player_name, motATrouver, motTrouve, resultat):
    """
    Nom de la fonction : add_to_stats_sheet
    Objet : inscrit les score de la partie dans un fichier au format txt
    :Params = 4: string
    :Return : les parmaètres dans un fichier text au format param1,parm2,param3,param3
    """
    with open("liste_resultats_pendu.txt", "a") as file:
        file.write(
            player_name + "," + motATrouver + "," + motTrouve + "," + resultat + "\n"
        )


def NouvellePartie():
    """
    Nom de la fonction : NouvellePartie
    Objet : permet au joueur de recommencer une nouvelle partie, en retounant au début
    par :
    return :
    """
    # print("jeu du pendu")
    reponse = input("Voulez_vous commencer une nouvelle partie oui/non ? \n").lower()
    print(reponse)
    if reponse == "oui":
        on_continue = True
    else:
        on_continue = False
        sys.exit("Vous avez quitté le jeu")
    return on_continue
