WORKING_DIR=$(git rev-parse --show-toplevel)
echo $WORKING_DIR
echo "#######################"
echo "-------RUN BLACK-------"
echo "#######################"
python -m black src

echo "#######################"
echo "-------RUN FLAKE 8-------"
echo "#######################"
python -m flake8 src


echo "#######################"
echo "-------RUN MYPY-------"
echo "#######################"
python -m mypy src



echo "#######################"
echo "-------RUN PYTEST-------"
echo "#######################"
python -m pytest src .