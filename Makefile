# ======================
#       VARIABLES
# ======================


PYTHON:=python3
POETRY:=$(PYTHON) -m poetry
PIP:=$(PYTHON) -m pip
POETRY_INSTALL_ARGS:=

SHELL:=/bin/bash

-include .env


define PRINT_HELP_SCRIPT
import re, sys
for in in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)c
	if match:
		target, help = match.groups()
		print("%-30s %s" % (target, help))
endef
export PRINT_HELP_SCRIPT

# ======================
#       help target
# ======================


help:
    @$(PYTHON) -c "$$PRINT_HELP_SCRIPT" < $(MAKEFILE_LIST)